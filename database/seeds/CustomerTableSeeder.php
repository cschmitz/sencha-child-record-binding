<?php

use Illuminate\Database\Seeder;
use App\Customer;
// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class CustomerTableSeeder extends Seeder
{
    protected $customer;

    public function __construct(Customer $customer){
        $this->customer = $customer;
    }

    public function run()
    {
        DB::table('customers')->delete();

        $this->customer->create([
            'id'    => 1,
            'name'  => 'Chris Schmitz',
            'phone' => '314.650.8682'
        ]);

        $this->customer->create([
            'id'    => 2,
            'name'  => 'Ruthie Whittam',
            'phone' => '314.444.1234'
        ]);
    }
}
