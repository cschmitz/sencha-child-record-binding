/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BindingChildSessionDemo.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.MessageBox'
    ],

    alias: 'controller.main',

    createDialog: function (record){
        // var view = this.getView();
        var tabs = Ext.getCmp('mainTabPanel');
        
        // sets a new property to the viewcontroller class
        this.isEdit = !!record;

        debugger;
        // adds a new component to the view we just grabbed and set it into a variable so we can act on it later
        this.tab = tabs.add({
            xtype: 'binding-child-session-form',
            viewModel:{
                data:{
                    title: record ? 'Edit: ' + record.get('name') : 'Add Customer'
                },

                links: {
                    theCustomer: record || {
                        type: 'Customer',
                        create: true
                    }
                }
            },

            session: true
        });

        tabs.setActiveItem(this.tab);
    },

    onEditCustomerClick: function (button){
        this.createDialog( button.getWidgetRecord() );
    },

    onAddCustomerClick: function (){
        this.createDialog(null);
    },

    onRemoveCustomerClick: function (){
        alert('Remove');
    },

    onSaveClick: function (a,b,c,d,e){
        debugger;
        var tab = this.tab;
    }
});
