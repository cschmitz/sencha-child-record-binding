/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('BindingChildSessionDemo.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'BindingChildSessionDemo.view.main.MainController',
        'BindingChildSessionDemo.view.main.MainModel',
        'BindingChildSessionDemo.view.customers.List',
        'BindingChildSessionDemo.view.customers.ChildSessionForm'
    ],

    xtype: 'app-main',
    
    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    session:true,

    items: [{
        xtype: 'panel',
        title: 'All Customers',
        region: 'west',
        width: 250,
        split: true,
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items:[
            {
                xtype: 'customergrid',
                flex: 1
            }
        ]
    },{
        region: 'center',
        xtype: 'tabpanel',
        id: 'mainTabPanel',
        items:[
            // {
            //     title: 'Tab 1',
            //     closable: true,
            //     html: '<h2>Select a customer from the list to view/edit.</h2>'
            // }
        ]
    }]
});
