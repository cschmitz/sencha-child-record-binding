
Ext.define("BindingChildSessionDemo.view.customers.ChildSessionForm",{
    extend: "Ext.panel.Panel",

    xtype: 'binding-child-session-form',

    // Note: we *can't* set this declaration. If we do, then when we have data being passed in we get *a new instance of the main controller*.
    // I don't know why, and I don't know why we get the same instance if there *isn't* data being passed in, but regardless, we want to *not*
    // declare the controller config so that we naturally inherit the mainController.
    // controller: 'main',

    bind:{
        title: '{title}'
    },
    layout: 'fit',
    closable:true,

    items:{
        xtype: 'form',
        reference: 'form',
        id: 'myform',
        bodyPadding: 10,
        border: false, 
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        items:[
            {
                xtype: 'textfield',
                fieldLabel: 'Name',
                reference: 'name',
                msgTarget: 'side',
                bind: '{theCustomer.name}'
            },
            {
                xtype: 'textfield',
                fieldLabel: 'Phone',
                reference: 'phone',
                msgTarget: 'side',
                bind: '{theCustomer.phone}'
            },
        ]
    },
    buttons:[
        {
            text: 'Save',
            handler: 'onSaveClick'
        },
        {
            text: 'Cancel',
            handler: 'onCancelClick'
        }
    ]

});
