
Ext.define("BindingChildSessionDemo.view.customers.List",{
    extend: "Ext.grid.Panel",

    requires: [
        "BindingChildSessionDemo.view.customers.ListModel"
    ],


    viewModel: {
        type: "customers-list"
    },
    reference: 'customerGrid',

    xtype: 'customergrid',

    bind:{
        store: '{Customers}'
    },

    dockedItems:[
        {
            type: 'toolbar',
            text: 'Add Customer',
            dock: 'bottom',
            layout: 'hbox',
            defaults:{width: '100px'},
            items:[
                {
                    xtype: 'button',
                    text: 'Add',
                    handler: 'onAddCustomerClick'
                }, 
                {
                    xtype: 'tbfill'
                },
                {
                    xtype: 'button',
                    text: 'Remove',
                    handler: 'onRemoveCustomerClick'
                }, 
            ]
        }
    ],

    columns:[
        {text: 'Name', dataIndex: 'name', flex: 1},
        {text: 'Phone', dataIndex: 'phone'},
        {
            xtype: 'widgetcolumn',
            width: 90,
            widget:{
                xtype: 'button',
                text: 'Edit',
                handler: 'onEditCustomerClick'
            }
        }
    ]
});
