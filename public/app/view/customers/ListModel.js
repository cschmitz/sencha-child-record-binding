Ext.define('BindingChildSessionDemo.view.customers.ListModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.customers-list',


    requires:[
        'BindingChildSessionDemo.model.Customer'
    ],

    stores:{
        Customers:{
            model: 'Customer',
            session: true,
            autoLoad: true
        }
        
    }

});
