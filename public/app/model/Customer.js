Ext.define('BindingChildSessionDemo.model.Customer', {
    extend: 'BindingChildSessionDemo.model.Base',
    
    fields: [
        { name: 'id', type: 'int' },
        { name: 'name', type: 'string' },
        { name: 'phone', type: 'string' }
    ],
    proxy:{
        type: 'rest',
        url: 'customers',
        reader: {
            type: 'json'
        }
    }
});
